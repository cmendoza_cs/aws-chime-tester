const AWS = require('aws-sdk');
const { v4: uuid } = require('uuid');

exports.handler = async function(event, context) {
  console.log("chime-create request: ", event);

  if (event.httpMethod != 'POST') {
    return {
      statusCode: 405
    };
  }

  const awsCred = new AWS.Credentials({
    accessKeyId: process.env.CHIME_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.CHIME_AWS_ACCESS_KEY_SECRET,
  });

  AWS.config = new AWS.Config({
    credentials: awsCred
  })

  // You must use "us-east-1" as the region for Chime API and set the endpoint.
  const chime = new AWS.Chime({ 
    region: 'us-east-1', 
  });
  chime.endpoint = new AWS.Endpoint('https://service.chime.aws.amazon.com');

  const meetingObj = await chime.createMeeting({
    ClientRequestToken: uuid(),
    MediaRegion: 'us-west-2' // Specify the region in which to create the meeting.
  }).promise();

  // your server-side functionality
  return {
    statusCode: 200,
    body: JSON.stringify(meetingObj)
  };
}